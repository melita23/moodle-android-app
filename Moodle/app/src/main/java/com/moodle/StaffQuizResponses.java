package com.moodle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class StaffQuizResponses extends AppCompatActivity {
    String quiz_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_quiz_responses);

        //Get quiz_url
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        quiz_url = bundle.getString("quiz_url");

        String[] url_split = quiz_url.split("/");
        getSupportActionBar().setTitle(url_split[url_split.length-3]+": Quiz Responses");
    }
}